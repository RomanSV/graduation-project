#!/bin/bash

rm -rf bin

mkdir bin
mkdir build
cd build
cmake ../core/
make
cp libmathcore.so ../bin/libmathcore.so
cd ..
rm -rf build