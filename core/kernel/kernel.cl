#define PITCH_NUM 88
#define E_MAX 5
#define E_MIN -5
#define D_MAX 10
#define L 16

void kernel calc_transition_probs(
        int n, int pn, int on, int en,
        global double *f0, global int *keys,
        int tatumCount, int fpt, int currentPitch,
        global double *phi, double sigma, global double *alpha,
        global double *result, local double *reductionSums
) {
    const int alphaSize = PITCH_NUM * tatumCount * (E_MAX - E_MIN + 1) * D_MAX;

    const int j = get_global_id(0);
    const int localId = get_local_id(0);
    const int localSize = get_local_size(0);
    const int pon = j / (D_MAX * (E_MAX - E_MIN + 1) * tatumCount);
    const int oon = (j / (D_MAX * (E_MAX - E_MIN + 1))) % tatumCount;
    const int eon = (j / D_MAX) % (E_MAX - E_MIN + 1);
    const int don = j % D_MAX;

    if (pon < (currentPitch - 3) || pon > currentPitch + 3 || on <= oon) {
        reductionSums[localId] = 0;
    } else {
        const int ta = (on * fpt) + eon;
        const int tb = ta + don;
        const int tc = (n * fpt) + en;

        const double ponF = pon * 100.0;
        const double pnF = pn * 100.0;

        double chi = 1;
        for (int t = ta; t < tc; t++) {
            const double mu = t < tb ? ponF + (pnF - ponF) * (t - ta) / don : pnF;
            const double tmp = (f0[t] - mu) / sigma;
            const double pdf = 10000 / (M_PI * sigma * (1 + tmp * tmp));
            chi *= pdf;
        }

        const double lambda = 1;
        const int key = keys[oon / L];
        const double transitionProb = lambda * phi[pn + PITCH_NUM * (pon + PITCH_NUM * key)] * chi;

        reductionSums[localId] = alpha[(on - 1) * alphaSize + j] * transitionProb;
    }

    for (int offset = localSize / 2; offset > 0; offset /= 2) {
        barrier(CLK_LOCAL_MEM_FENCE);
        if (localId < offset) {
            reductionSums[localId] += reductionSums[localId + offset];
        }
    }

    if (localId == 0) {
        result[get_group_id(0)] = reductionSums[0];
    }

}