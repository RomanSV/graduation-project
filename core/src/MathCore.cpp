#include "MathCore.h"

MathCore::MathCore(const std::string &kernelPath, int deviceGroupSize)
        : program(ClHelper::createProgram(kernelPath)), deviceGroupSize(deviceGroupSize) {
    auto context = program.getInfo<CL_PROGRAM_CONTEXT>();
    auto device = context.getInfo<CL_CONTEXT_DEVICES>().front();
    kernel = cl::Kernel(program, "calc_transition_probs");
    queue = cl::CommandQueue(context, device);
}

int MathCore::freqToPitch(double freq) {
    return (int) (std::round(freq / 100));
}

double MathCore::pitchToFreq(int pitch) {
    return pitch * 100.0;
}

void MathCore::calcTransitionProbs(
        int n, int pn, int on, int en, int tatumCount, int currentPitch, double *result
) {
    const int alphaSize = PITCH_NUM * tatumCount * (E_MAX - E_MIN + 1) * D_MAX;

    kernel.setArg(0, n);
    kernel.setArg(1, pn);
    kernel.setArg(2, on);
    kernel.setArg(3, en);
    kernel.setArg(8, currentPitch);

    queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(alphaSize), cl::NDRange(deviceGroupSize));

    const int outSize = alphaSize / deviceGroupSize;
    queue.enqueueReadBuffer(*buffer_result, CL_TRUE, 0, sizeof(double) * outSize, result);
}

void MathCore::calcTatumAlpha(
        int n, double *alpha, const double *f0, const int *nearbyPitches, int nearbyPitchesSize, int previousPitch,
        int tatumCount, int fpt, const double *epsilon, const double *delta, double sigma
) {
    const int alphaSize = PITCH_NUM * tatumCount * (E_MAX - E_MIN + 1) * D_MAX;

    auto context = program.getInfo<CL_PROGRAM_CONTEXT>();

    const int outSize = alphaSize / deviceGroupSize;

    // create queue to which we will push commands for the device
    queue.enqueueWriteBuffer(*buffer_alpha, CL_FALSE, 0, sizeof(double) * alphaSize * tatumCount, alpha);

    kernel.setArg(6, tatumCount);
    kernel.setArg(7, fpt);
    kernel.setArg(10, sigma);
    kernel.setArg(11, *buffer_alpha);
    kernel.setArg(12, *buffer_result);
    kernel.setArg(13, cl::Local(sizeof(double) * deviceGroupSize));

    int minP = nearbyPitches[0];
    int maxP = nearbyPitches[0];

    for (int i = 1; i < nearbyPitchesSize; i++) {
        if (nearbyPitches[i] < minP) {
            minP = nearbyPitches[i];
        }
        if (nearbyPitches[i] > maxP) {
            maxP = nearbyPitches[i];
        }
    }

    auto *computeResult = (double *) std::malloc(outSize * sizeof(double));
    double alphaSum = 0;
    for (int pn = minP - 1; pn <= maxP + 1; pn++) {
        for (int on = std::max(1, n - L); on < n; on++) {
            for (int en = E_MIN; en <= E_MAX; en++) {
                for (int dn = 1; dn <= D_MAX; dn++) {
                    calcTransitionProbs(n, pn, on, en, tatumCount, previousPitch, computeResult);

                    const int j = (dn - 1) + D_MAX * ((en - E_MIN) + (E_MAX - E_MIN + 1) * (on + tatumCount * pn));
                    double alphaDotTransitions = 0;
                    for (int k = 0; k < outSize; k++) {
                        alphaDotTransitions += computeResult[k];
                    }
                    const double alphaJ = alphaDotTransitions * epsilon[en - E_MIN] * delta[dn - 1];
                    alpha[(n - 1) * alphaSize + j] = alphaJ;
                    alphaSum += alphaJ;
                }
            }
        }
    }
    free(computeResult);

    for (int i = 0; i < alphaSize; i++) {
        alpha[(n - 1) * alphaSize + i] /= alphaSum;
    }
}

void MathCore::setIterationParameters(
        const double *f0, int f0Size, const int *keys, int keysSize, const double *phi, int tatumCount
) {
    auto context = program.getInfo<CL_PROGRAM_CONTEXT>();
    buffer_f0 = std::make_unique<cl::Buffer>(context, CL_MEM_READ_ONLY, sizeof(double) * f0Size);
    buffer_keys = std::make_unique<cl::Buffer>(context, CL_MEM_READ_ONLY, sizeof(int) * keysSize);
    buffer_phi = std::make_unique<cl::Buffer>(context, CL_MEM_READ_ONLY, sizeof(double) * PITCH_NUM * PITCH_NUM * 24);

    const int alphaSize = PITCH_NUM * tatumCount * (E_MAX - E_MIN + 1) * D_MAX;
    const int outSize = alphaSize / deviceGroupSize;
    buffer_result = std::make_unique<cl::Buffer>(context, CL_MEM_WRITE_ONLY, sizeof(double) * outSize);
    buffer_alpha = std::make_unique<cl::Buffer>(context, CL_MEM_READ_ONLY, sizeof(double) * alphaSize * tatumCount);

    queue.enqueueWriteBuffer(*buffer_f0, CL_FALSE, 0, sizeof(double) * f0Size, f0);
    queue.enqueueWriteBuffer(*buffer_keys, CL_FALSE, 0, sizeof(int) * keysSize, keys);
    queue.enqueueWriteBuffer(*buffer_phi, CL_FALSE, 0, sizeof(double) * PITCH_NUM * PITCH_NUM * 24, phi);
    queue.flush();

    kernel.setArg(4, *buffer_f0);
    kernel.setArg(5, *buffer_keys);
    kernel.setArg(9, *buffer_phi);
}

void MathCore::resetIterationParameters() {
    buffer_f0.reset();
    buffer_keys.reset();
    buffer_phi.reset();
    buffer_result.reset();
    buffer_alpha.reset();
}

void MathCore::calcBackwardProbs(
        const double *alpha, int n, int pn, int on, int en, int dn, const double *f0,
        const int *keys, int tatumCount, int fpt, const double *phi, const double *epsilon,
        const double *delta, double sigma, double *result
) {
    double freq = 0;
    for (int tatum = on; tatum <= n; tatum++) {
        freq += f0[tatum * fpt];
    }
    freq /= (n - on + 1);
    const int currentPitch = freqToPitch(freq);

    for (int pon = currentPitch - 2; pon <= currentPitch + 2; pon++) {
        const double ponF = pitchToFreq(pon);
        const double pnF = pitchToFreq(pn);

        for (int oon = std::max(0, on - L); oon < on; oon++) {
            const auto key = keys[(int) std::floor(oon / L)];
            const double lambda = 1;
            for (int eon = E_MIN; eon <= E_MAX; eon++) {
                const int ta = (on * fpt) + eon;
                const int tc = (n * fpt) + en;

                for (int don = 1; don <= D_MAX; don++) {
                    const int tb = ta + don;
                    if (tc <= tb)
                        continue;

                    auto mu = std::vector<double>(tc - ta, pnF);
                    for (int t = ta; t < tb; t++) {
                        mu[t - ta] = ponF + (pnF - ponF) * (t - ta) / don;
                    }

                    double chi = 1;
                    for (int t = ta; t < tc; t++) {
                        const double test = (f0[t] - mu[t - ta]) / sigma;
                        const double pdf = 10000 / (M_PI * sigma * (1 + test * test));
                        chi *= pdf;
                    }
                    const double transitionProb =
                            epsilon[en] * delta[dn] * lambda * phi[pn + PITCH_NUM * (pon + PITCH_NUM * key)] * chi;
                    const int index =
                            (don - 1) + D_MAX * ((eon - E_MIN) + (E_MAX - E_MIN + 1) * (oon + tatumCount * pon));
                    result[index] = transitionProb * alpha[index];
                }
            }
        }
    }
}

MathCore *MathCore_new() { return new MathCore("core/kernel/kernel.cl"); }

void MathCore_calcTatumAlpha(
        MathCore *mathCore, int n, double *alpha, const double *f0, const int *nearbyPitches, int nearbyPitchesSize,
        int previousPitch, int tatumCount, int fpt, const double *epsilon, const double *delta, double sigma
) {
    mathCore->calcTatumAlpha(
            n, alpha, f0, nearbyPitches, nearbyPitchesSize, previousPitch, tatumCount, fpt, epsilon, delta, sigma
    );
}

void MathCore_setIterationParameters(
        MathCore *mathCore, const double *f0, int f0Length, const int *keys, int keysLength, const double *phi, int tatumCount
) {
    mathCore->setIterationParameters(f0, f0Length, keys, keysLength, phi, tatumCount);
}

void MathCore_calcBackwardProbs(
        MathCore *mathCore, const double *alpha, int n, int pn, int on, int en, int dn,
        const double *f0, const int *keys, int tatumCount, int fpt,
        const double *phi, const double *epsilon, const double *delta, double sigma, double *result
) {
    mathCore->calcBackwardProbs(
            alpha, n, pn, on, en, dn, f0, keys, tatumCount, fpt, phi, epsilon, delta, sigma, result
    );
}

void MathCore_resetIterationParameters(MathCore *mathCore) {
    mathCore->resetIterationParameters();
}
