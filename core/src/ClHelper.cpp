#include "ClHelper.h"

cl::Program ClHelper::createProgram(const std::string &file) {
    //get all platforms (drivers)
    std::vector<cl::Platform> all_platforms;
    cl::Platform::get(&all_platforms);
    if (all_platforms.empty()) {
        std::printf(" No platforms found. Check OpenCL installation!\n");
        exit(1);
    }
    cl::Platform default_platform = all_platforms[0];
    std::printf("Using platform: %s\n", default_platform.getInfo<CL_PLATFORM_NAME>().c_str());

    //get default device of the default platform
    std::vector<cl::Device> all_devices;
    default_platform.getDevices(CL_DEVICE_TYPE_ALL, &all_devices);
    if (all_devices.empty()) {
        std::printf("No devices found. Check OpenCL installation!\n");
        exit(1);
    }
    cl::Device default_device = all_devices[0];
    std::printf("Using device: %s\n", default_device.getInfo<CL_DEVICE_NAME>().c_str());

    cl::Context context({default_device});
    cl::Program::Sources sources;

    std::ifstream kernelSrcFile(file);
    if (!kernelSrcFile.is_open()) {
        std::printf("Could not open the kernel file\n");
    }
    std::string kernelCode((std::istreambuf_iterator<char>(kernelSrcFile)), std::istreambuf_iterator<char>());

    sources.push_back({kernelCode.c_str(), kernelCode.length()});

    cl::Program program(context, sources);
    if (program.build({default_device}) != CL_SUCCESS) {
        std::printf("Error building.\n%s\n", program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(default_device).c_str());
        exit(1);
    }
    return program;
}
