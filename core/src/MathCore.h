#ifndef NB_MATHCOREGPU_H
#define NB_MATHCOREGPU_H

#include <cmath>
#include <memory>
#include "ClHelper.h"

class MathCore {
public:
    static constexpr int PITCH_NUM = 88;
    static constexpr int E_MAX = 5;
    static constexpr int E_MIN = -5;
    static constexpr int D_MAX = 10;
    static constexpr int L = 16;

    const int deviceGroupSize;

    explicit MathCore(const std::string &kernelPath) : MathCore(kernelPath, 32) {};

    MathCore(const std::string &kernelPath, int deviceGroupSize);

    void calcTatumAlpha(
            int n, double *alpha, const double *f0, const int *nearbyPitches, int nearbyPitchesSize,
            int previousPitch, int tatumCount, int fpt, const double *epsilon, const double *delta, double sigma
    );

    void setIterationParameters(
            const double *f0, int f0Size, const int *keys, int keysSize, const double *phi, int tatumCount
    );

    void resetIterationParameters();

    void calcTransitionProbs(
            int n, int pn, int on, int en, int tatumCount, int currentPitch, double *result
    );

    void calcBackwardProbs(
            const double *alpha, int n, int pn, int on, int en, int dn,
            const double *f0, const int *keys, int tatumCount, int fpt,
            const double *phi, const double *epsilon, const double *delta, double sigma,
            double *result
    );

private:
    cl::Program program;
    std::unique_ptr<cl::Buffer> buffer_f0;
    std::unique_ptr<cl::Buffer> buffer_keys;
    std::unique_ptr<cl::Buffer> buffer_phi;
    std::unique_ptr<cl::Buffer> buffer_result;
    std::unique_ptr<cl::Buffer> buffer_alpha;
    cl::CommandQueue queue;
    cl::Kernel kernel;

    static int freqToPitch(double freq);

    static double pitchToFreq(int pitch);
};

extern "C"
{
    MathCore *MathCore_new();

    void MathCore_calcTatumAlpha(
            MathCore *mathCore, int n, double *alpha, const double *f0, const int *nearbyPitches, int nearbyPitchesSize,
            int previousPitch, int tatumCount, int fpt, const double *epsilon, const double *delta, double sigma
    );

    void MathCore_setIterationParameters(
            MathCore *mathCore, const double *f0, int f0Length, const int *keys, int keysLength, const double *phi, int tatumCount
    );

    void MathCore_resetIterationParameters(MathCore *mathCore);

    void MathCore_calcBackwardProbs(
            MathCore *mathCore, const double *alpha, int n, int pn, int on, int en, int dn,
            const double *f0, const int *keys, int tatumCount, int fpt,
            const double *phi, const double *epsilon, const double *delta, double sigma,
            double *result
    );
}

#endif //NB_MATHCOREGPU_H
