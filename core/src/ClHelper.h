
#ifndef NB_CLHELPER_H
#define NB_CLHELPER_H

#include <string>
#include <fstream>
#include <CL/cl.hpp>

namespace ClHelper {
    cl::Program createProgram(const std::string &file);
}


#endif //NB_CLHELPER_H
