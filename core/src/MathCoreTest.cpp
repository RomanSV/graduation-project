#include <iostream>
#include "MathCore.h"

int main() {
    MathCore mathCore{"../kernel/kernel.cl", 64};
    std::vector<double> alpha(16 * MathCore::PITCH_NUM * 16 * (MathCore::E_MAX - MathCore::E_MIN + 1) * MathCore::D_MAX,
                              0);
    std::vector<double> f0(16 * 17, 4900.0);
    std::vector<int> keys(1, 21);
    std::vector<double> phi(24 * MathCore::PITCH_NUM * MathCore::PITCH_NUM, 1.0 / MathCore::PITCH_NUM);
    std::vector<double> epsilon((MathCore::E_MAX - MathCore::E_MIN + 1), 1.0 / (MathCore::E_MAX - MathCore::E_MIN + 1));
    std::vector<double> delta(MathCore::D_MAX, 1.0 / MathCore::D_MAX);
    std::vector<int> nearbyPitches(1, 49);
    mathCore.setIterationParameters(f0.data(), f0.size(), keys.data(), keys.size(), phi.data(), 16);
    mathCore.calcTatumAlpha(15, alpha.data(), f0.data(), nearbyPitches.data(), nearbyPitches.size(),49,
                            16, 17, epsilon.data(), delta.data(), 0.5);
    mathCore.resetIterationParameters();
    std::printf("Finished!\n");
    return 0;
}
