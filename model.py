from dataclasses import dataclass
import numpy as np

# Constants
PITCH_NUM = 88  # K in the paper
KEY_NUM = 24  # M in the paper: {C, C#, ... B} x {major, minor}
E_MIN = -5
E_MAX = 5
D_MAX = 10
L = 16  # tatums in one measure

KEYS_DICT = {
    0: "C",
    1: "C#",
    2: "D",
    3: "D#",
    4: "E",
    5: "F",
    6: "F#",
    7: "G",
    8: "G#",
    9: "A",
    10: "A#",
    11: "B",
    12: "C minor",
    13: "C# minor",
    14: "D minor",
    15: "D# minor",
    16: "E minor",
    17: "F minor",
    18: "F# minor",
    19: "G minor",
    20: "G# minor",
    21: "A minor",
    22: "A# minor",
    23: "B minor",
}

@dataclass
class Note:
    pitch: int
    onset: int
    onset_deviation: int
    transient_duration: int


@dataclass
class ModelParameters:
    phi_init: np.ndarray  # [KEY_NUM, PITCH_NUM]  # Key to pitch emission
    phi: np.ndarray  # [KEY_NUM, PITCH_NUM, PITCH_NUM]  # Key to pitch emission given previous pitch
    pi_init: np.ndarray  # [KEY_NUM]  # Key initial probabilities
    pi: np.ndarray  # [KEY_NUM, KEY_NUM]  # Key transition probabilities
    lamb: np.ndarray  # [L, L]  # tatum positions transition probabilities
    epsilon: np.ndarray  # [E_MAX - E_MIN + 1]
    delta: np.ndarray  # [D_MAX]
    sigma: np.float  # scalar
    # count data
    c_pi_init: np.ndarray
    c_pi: np.ndarray
    c_phi_init: np.ndarray
    c_phi: np.ndarray
    c_epsilon: np.ndarray
    c_delta: np.ndarray