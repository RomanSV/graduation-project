from ctypes import *
import numpy as np
from model import *


class MathCore(object):
    lib = cdll.LoadLibrary('./bin/libmathcore.so')
    
    def __init__(self):
        MathCore.lib.MathCore_new.restype = c_void_p

        MathCore.lib.MathCore_calcTatumAlpha.argtypes = [
            c_void_p, c_int, POINTER(c_double), POINTER(c_double),
            POINTER(c_int), c_int, c_int,
            c_int, c_int, POINTER(c_double), POINTER(c_double), c_double
        ]
        MathCore.lib.MathCore_calcTatumAlpha.restype = None

        MathCore.lib.MathCore_setIterationParameters.argtypes = [
            c_void_p, POINTER(c_double), c_int, POINTER(c_int), c_int, POINTER(c_double), c_int
        ]
        MathCore.lib.MathCore_setIterationParameters.restype = None

        MathCore.lib.MathCore_resetIterationParameters.argtypes = [c_void_p]
        MathCore.lib.MathCore_resetIterationParameters.restype = None

        
        MathCore.lib.MathCore_calcBackwardProbs.argtypes = [
            c_void_p, POINTER(c_double), c_int, c_int, c_int, c_int, c_int,
            POINTER(c_double), POINTER(c_int), c_int, c_int,
            POINTER(c_double), POINTER(c_double), POINTER(c_double), c_double, POINTER(c_double)
        ]
        MathCore.lib.MathCore_calcBackwardProbs.restype = None

        self.obj = MathCore.lib.MathCore_new()

    def calc_tatum_alpha(self, n, alpha, f0, nearbyPitches, previousPitch, tatum_count, fpt, theta: ModelParameters):
        alpha_c = alpha.flatten()
        MathCore.lib.MathCore_calcTatumAlpha(
            self.obj, n, alpha_c.ctypes.data_as(POINTER(c_double)),
            np.float64(f0).ctypes.data_as(POINTER(c_double)), 
            nearbyPitches.ctypes.data_as(POINTER(c_int)), len(nearbyPitches), previousPitch,
            tatum_count, fpt, 
            theta.epsilon.ctypes.data_as(POINTER(c_double)),
            theta.delta.ctypes.data_as(POINTER(c_double)), theta.sigma
        )
        return alpha_c.reshape(alpha.shape)

    def set_iteration_parameters(self, f0, keys, theta: ModelParameters, tatum_count: int):
        phi = theta.phi.flatten()
        MathCore.lib.MathCore_setIterationParameters(
            self.obj, np.float64(f0).ctypes.data_as(POINTER(c_double)), f0.shape[0], 
            keys.ctypes.data_as(POINTER(c_int)), keys.shape[0], phi.ctypes.data_as(POINTER(c_double)), tatum_count
        )

    def reset_iteration_parameters(self):
        MathCore.lib.MathCore_resetIterationParameters(self.obj)

    def calc_backward_probs(self, alpha, n, pn, on, en, dn, f0, keys, tatum_count, fpt, theta: ModelParameters, result):
        phi = theta.phi.flatten()
        MathCore.lib.MathCore_calcBackwardProbs(
            self.obj, alpha.ctypes.data_as(POINTER(c_double)), n, pn, on, en, dn,
            np.float64(f0).ctypes.data_as(POINTER(c_double)), keys.ctypes.data_as(POINTER(c_int)),
            tatum_count, fpt, phi.ctypes.data_as(POINTER(c_double)), theta.epsilon.ctypes.data_as(POINTER(c_double)),
            theta.delta.ctypes.data_as(POINTER(c_double)), theta.sigma, result.ctypes.data_as(POINTER(c_double))
        )
